package com.atlassian.hibernate.adapter.adapters;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.connection.ConnectionProvider;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * An adapter bridging the ConnectionProvider interface for hibernate v5 (onwards) to v2.
 */
public class ConnectionProviderV2Adapter implements ConnectionProvider {
    private final org.hibernate.engine.jdbc.connections.spi.ConnectionProvider connectionProvider;

    protected ConnectionProviderV2Adapter(final org.hibernate.engine.jdbc.connections.spi.ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public static ConnectionProvider adapt(final org.hibernate.engine.jdbc.connections.spi.ConnectionProvider connectionProvider) {
        if (connectionProvider == null) {
            return null;
        }
        if (connectionProvider instanceof ConnectionProviderV5Adapter) {
            return ((ConnectionProviderV5Adapter) connectionProvider).getV2ConnectionProvider();
        }
        return new ConnectionProviderV2Adapter(connectionProvider);
    }

    public org.hibernate.engine.jdbc.connections.spi.ConnectionProvider getV5ConnectionProvider() {
        return connectionProvider;
    }

    @Override
    public void configure(final Properties props) throws HibernateException {
    }

    @Override
    public Connection getConnection() throws SQLException {
        return connectionProvider.getConnection();
    }

    @Override
    public void closeConnection(final Connection conn) throws SQLException {
        connectionProvider.closeConnection(conn);
    }

    @Override
    public void close() throws HibernateException {
    }
}
