package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.cache.spi.access.CollectionRegionAccessStrategy;
import org.hibernate.cache.spi.entry.CacheEntryStructure;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.persister.collection.CollectionPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.persister.walking.spi.CollectionElementDefinition;
import org.hibernate.persister.walking.spi.CollectionIndexDefinition;
import org.hibernate.type.CollectionType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * An adapter bridging the CollectionPersister interface for hibernate v2 to v5 (onwards).
 */
public class CollectionPersisterV5Adapter implements CollectionPersister {
    private final net.sf.hibernate.collection.CollectionPersister persister;
    private final org.hibernate.SessionFactory sessionFactory;

    protected CollectionPersisterV5Adapter(
            final net.sf.hibernate.collection.CollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        this.persister = persister;
        this.sessionFactory = sessionFactory;
    }

    public static CollectionPersister adapt(
            final net.sf.hibernate.collection.CollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        if (persister == null)
            return null;
        return new CollectionPersisterV5Adapter(persister, sessionFactory);
    }

    @Override
    public Type getElementType() {
        return TypeV5Adapter.adapt(sessionFactory, persister.getElementType());
    }

    @Override
    public Class getElementClass() {
        return persister.getElementClass();
    }

    //---------- Not Implemented ----------//

    @Override
    public void initialize(final Serializable key, final SharedSessionContractImplementor session) throws HibernateException {
        throw new NotImplementedException("initialize not implemented");
    }

    @Override
    public boolean hasCache() {
        throw new NotImplementedException("hasCache not implemented");
    }

    @Override
    public CollectionRegionAccessStrategy getCacheAccessStrategy() {
        throw new NotImplementedException("getCacheAccessStrategy not implemented");
    }

    @Override
    public CacheEntryStructure getCacheEntryStructure() {
        throw new NotImplementedException("getCacheEntryStructure not implemented");
    }

    @Override
    public CollectionPersister getCollectionPersister() {
        throw new NotImplementedException("getCollectionPersister not implemented");
    }

    @Override
    public CollectionType getCollectionType() {
        throw new NotImplementedException("getCollectionType not implemented");
    }

    @Override
    public CollectionIndexDefinition getIndexDefinition() {
        throw new NotImplementedException("getIndexDefinition not implemented");
    }

    @Override
    public CollectionElementDefinition getElementDefinition() {
        throw new NotImplementedException("getElementDefinition not implemented");
    }

    @Override
    public Type getKeyType() {
        throw new NotImplementedException("getKeyType not implemented");
    }

    @Override
    public Type getIndexType() {
        throw new NotImplementedException("getIndexType not implemented");
    }

    @Override
    public Object readKey(final ResultSet rs, final String[] keyAliases, final SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        throw new NotImplementedException("readKey not implemented");
    }

    @Override
    public Object readElement(final ResultSet rs, final Object owner, final String[] columnAliases, final SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        throw new NotImplementedException("readElement not implemented");
    }

    @Override
    public Object readIndex(final ResultSet rs, final String[] columnAliases, final SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        throw new NotImplementedException("readIndex not implemented");
    }

    @Override
    public Object readIdentifier(final ResultSet rs, final String columnAlias, final SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        throw new NotImplementedException("readIdentifier not implemented");
    }

    @Override
    public boolean isPrimitiveArray() {
        throw new NotImplementedException("isPrimitiveArray not implemented");
    }

    @Override
    public boolean isArray() {
        throw new NotImplementedException("isArray not implemented");
    }

    @Override
    public boolean isOneToMany() {
        throw new NotImplementedException("isOneToMany not implemented");
    }

    @Override
    public boolean isManyToMany() {
        throw new NotImplementedException("isManyToMany not implemented");
    }

    @Override
    public String getManyToManyFilterFragment(final String alias, final Map enabledFilters) {
        throw new NotImplementedException("getManyToManyFilterFragment not implemented");
    }

    @Override
    public boolean hasIndex() {
        throw new NotImplementedException("hasIndex not implemented");
    }

    @Override
    public boolean isLazy() {
        throw new NotImplementedException("isLazy not implemented");
    }

    @Override
    public boolean isInverse() {
        throw new NotImplementedException("isInverse not implemented");
    }

    @Override
    public void remove(final Serializable id, final SharedSessionContractImplementor session) throws HibernateException {
        throw new NotImplementedException("remove not implemented");
    }

    @Override
    public void recreate(final PersistentCollection collection, final Serializable key, final SharedSessionContractImplementor session)
            throws HibernateException {
        throw new NotImplementedException("recreate not implemented");
    }

    @Override
    public void deleteRows(final PersistentCollection collection, final Serializable key, final SharedSessionContractImplementor session)
            throws HibernateException {
        throw new NotImplementedException("deleteRows not implemented");
    }

    @Override
    public void updateRows(final PersistentCollection collection, final Serializable key, final SharedSessionContractImplementor session)
            throws HibernateException {
        throw new NotImplementedException("updateRows not implemented");
    }

    @Override
    public void insertRows(final PersistentCollection collection, final Serializable key, final SharedSessionContractImplementor session)
            throws HibernateException {
        throw new NotImplementedException("insertRows not implemented");
    }

    @Override
    public void processQueuedOps(final PersistentCollection collection, final Serializable key, final SharedSessionContractImplementor session)
            throws HibernateException {
        throw new NotImplementedException("processQueuedOps not implemented");
    }

    @Override
    public String getRole() {
        throw new NotImplementedException("getRole not implemented");
    }

    @Override
    public EntityPersister getOwnerEntityPersister() {
        throw new NotImplementedException("getOwnerEntityPersister not implemented");
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator() {
        throw new NotImplementedException("getIdentifierGenerator not implemented");
    }

    @Override
    public Type getIdentifierType() {
        throw new NotImplementedException("getIdentifierType not implemented");
    }

    @Override
    public boolean hasOrphanDelete() {
        throw new NotImplementedException("hasOrphanDelete not implemented");
    }

    @Override
    public boolean hasOrdering() {
        throw new NotImplementedException("hasOrdering not implemented");
    }

    @Override
    public boolean hasManyToManyOrdering() {
        throw new NotImplementedException("hasManyToManyOrdering not implemented");
    }

    @Override
    public Serializable[] getCollectionSpaces() {
        throw new NotImplementedException("getCollectionSpaces not implemented");
    }

    @Override
    public CollectionMetadata getCollectionMetadata() {
        throw new NotImplementedException("getCollectionMetadata not implemented");
    }

    @Override
    public boolean isCascadeDeleteEnabled() {
        throw new NotImplementedException("isCascadeDeleteEnabled not implemented");
    }

    @Override
    public boolean isVersioned() {
        throw new NotImplementedException("isVersioned not implemented");
    }

    @Override
    public boolean isMutable() {
        throw new NotImplementedException("isMutable not implemented");
    }

    @Override
    public void postInstantiate() throws MappingException {
        throw new NotImplementedException("postInstantiate not implemented");
    }

    @Override
    public SessionFactoryImplementor getFactory() {
        throw new NotImplementedException("getFactory not implemented");
    }

    @Override
    public boolean isAffectedByEnabledFilters(final SharedSessionContractImplementor session) {
        throw new NotImplementedException("isAffectedByEnabledFilters not implemented");
    }

    @Override
    public String[] getKeyColumnAliases(final String suffix) {
        throw new NotImplementedException("getKeyColumnAliases not implemented");
    }

    @Override
    public String[] getIndexColumnAliases(final String suffix) {
        throw new NotImplementedException("getIndexColumnAliases not implemented");
    }

    @Override
    public String[] getElementColumnAliases(final String suffix) {
        throw new NotImplementedException("getElementColumnAliases not implemented");
    }

    @Override
    public String getIdentifierColumnAlias(final String suffix) {
        throw new NotImplementedException("getIdentifierColumnAlias not implemented");
    }

    @Override
    public boolean isExtraLazy() {
        throw new NotImplementedException("isExtraLazy not implemented");
    }

    @Override
    public int getSize(final Serializable key, final SharedSessionContractImplementor session) {
        throw new NotImplementedException("getSize not implemented");
    }

    @Override
    public boolean indexExists(final Serializable key, final Object index, final SharedSessionContractImplementor session) {
        throw new NotImplementedException("indexExists not implemented");
    }

    @Override
    public boolean elementExists(final Serializable key, final Object element, final SharedSessionContractImplementor session) {
        throw new NotImplementedException("elementExists not implemented");
    }

    @Override
    public Object getElementByIndex(final Serializable key, final Object index, final SharedSessionContractImplementor session, final Object owner) {
        throw new NotImplementedException("getElementByIndex not implemented");
    }

    @Override
    public int getBatchSize() {
        throw new NotImplementedException("getBatchSize not implemented");
    }

    @Override
    public String getMappedByProperty() {
        throw new NotImplementedException("getMappedByProperty not implemented");
    }
}
