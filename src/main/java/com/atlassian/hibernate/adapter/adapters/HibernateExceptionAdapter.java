package com.atlassian.hibernate.adapter.adapters;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.WeakHashMap;

/**
 * An adapter bridging the HibernateExceptions between hibernate v2 / v5 (onwards).
 */
public final class HibernateExceptionAdapter {

    private static WeakHashMap<Exception, Exception> exceptionMap = new WeakHashMap<>();

    private HibernateExceptionAdapter() { }

    public static net.sf.hibernate.HibernateException toV2HibernateException(
            final net.sf.hibernate.SessionFactory sessionFactoryV2,
            final SQLException ex,
            final String message) {

        return net.sf.hibernate.exception.JDBCExceptionHelper.convert(
                sessionFactoryV2.getSQLExceptionConverter(), ex, message);
    }

    public static PersistenceException toV5HibernateException(final SQLException ex, final String message) {
        return new SqlExceptionHelper(true).convert(ex, message);
    }

    public static net.sf.hibernate.HibernateException adapt(final PersistenceException ex) {
        // if available, get the original exception that was previously adapted
        net.sf.hibernate.HibernateException result;
        synchronized (exceptionMap) {
            result = (net.sf.hibernate.HibernateException) exceptionMap.get(ex);
        }

        if (result == null) {
            result = adaptImpl(ex);
            result.setStackTrace(ex.getStackTrace());
            synchronized (exceptionMap) {
                exceptionMap.put(result, ex);
            }
        }
        return result;
    }

    private static net.sf.hibernate.HibernateException adaptImpl(final PersistenceException ex) {
        // ExceptionConverterImpl strangely wraps some PersistenceExceptions in a PersistenceException
        if (ex.getClass() == PersistenceException.class && ex.getCause() != ex && ex.getCause() instanceof PersistenceException)
            return adaptImpl((PersistenceException) ex.getCause());

        if (ex instanceof javax.persistence.EntityExistsException)
            return adaptNonUniqueObjectException((javax.persistence.EntityExistsException) ex);
        if (ex instanceof javax.persistence.NonUniqueResultException)
            return adaptNonUniqueResultException(ex);
        if (ex instanceof javax.persistence.EntityNotFoundException)
            return adaptObjectNotFoundException((javax.persistence.EntityNotFoundException) ex);

        if (ex instanceof org.hibernate.cache.CacheException)
            return adaptCacheException((org.hibernate.cache.CacheException) ex);
        if (ex instanceof org.hibernate.CallbackException)
            return new net.sf.hibernate.CallbackException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof org.hibernate.exception.ConstraintViolationException)
            return adaptConstraintException((org.hibernate.exception.ConstraintViolationException) ex);
        if (ex instanceof org.hibernate.exception.GenericJDBCException)
            return new net.sf.hibernate.exception.GenericJDBCException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof org.hibernate.id.IdentifierGenerationException)
            return new net.sf.hibernate.id.IdentifierGenerationException(ex.getMessage(), ex.getCause());
        if (ex instanceof org.hibernate.InstantiationException)
            return adaptInstantiationException((org.hibernate.InstantiationException) ex);
        if (ex instanceof org.hibernate.exception.JDBCConnectionException)
            return new net.sf.hibernate.exception.JDBCConnectionException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof org.hibernate.exception.LockAcquisitionException)
            return new net.sf.hibernate.exception.LockAcquisitionException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof org.hibernate.NonUniqueObjectException)
            return adaptNonUniqueObjectException((org.hibernate.NonUniqueObjectException) ex);
        if (ex instanceof org.hibernate.NonUniqueResultException)
            return adaptNonUniqueResultException(ex);
        if (ex instanceof org.hibernate.ObjectDeletedException)
            return adaptObjectDeletedException((org.hibernate.ObjectDeletedException) ex);
        if (ex instanceof org.hibernate.ObjectNotFoundException)
            return adaptObjectNotFoundException((org.hibernate.ObjectNotFoundException) ex);
        if (ex instanceof org.hibernate.PersistentObjectException)
            return new net.sf.hibernate.PersistentObjectException(ex.getMessage());
        if (ex instanceof org.hibernate.PropertyAccessException)
            return adaptPropertyAccessException((org.hibernate.PropertyAccessException) ex);
        if (ex instanceof org.hibernate.PropertyNotFoundException)
            return new net.sf.hibernate.PropertyNotFoundException(ex.getMessage());
        if (ex instanceof org.hibernate.PropertyValueException)
            return adaptPropertyValueException((org.hibernate.PropertyValueException) ex);
        if (ex instanceof org.hibernate.QueryException)
            return adaptQueryException((org.hibernate.QueryException) ex);
        if (ex instanceof org.hibernate.exception.SQLGrammarException)
            return new net.sf.hibernate.exception.SQLGrammarException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof org.hibernate.type.SerializationException)
            return new net.sf.hibernate.type.SerializationException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof org.hibernate.StaleStateException)
            return adaptStaleObjectStateException((org.hibernate.StaleStateException) ex);
        if (ex instanceof javax.persistence.OptimisticLockException)
            return adaptOptimisticLockException((javax.persistence.OptimisticLockException) ex);
        if (ex instanceof org.hibernate.TransactionException)
            return new net.sf.hibernate.TransactionException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof org.hibernate.TransientObjectException)
            return new net.sf.hibernate.TransientObjectException(ex.getMessage());
        if (ex instanceof org.hibernate.UnresolvableObjectException)
            return adaptUnresolvableObjectException((org.hibernate.UnresolvableObjectException) ex);
        if (ex instanceof org.hibernate.WrongClassException)
            return adaptWrongClassException((org.hibernate.WrongClassException) ex);
        if (ex instanceof org.hibernate.JDBCException)
            return new net.sf.hibernate.JDBCException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof org.hibernate.MappingException)
            return new net.sf.hibernate.MappingException(ex.getMessage(), ex.getCause());
        return new net.sf.hibernate.HibernateException(ex.getMessage(), ex.getCause());
    }

    public static PersistenceException adapt(final net.sf.hibernate.HibernateException ex) {
        // if available, get the original exception that was previously adapted
        PersistenceException result;
        synchronized (exceptionMap) {
            result = (PersistenceException) exceptionMap.get(ex);
        }

        if (result == null) {
            result = adaptImpl(ex);
            result.setStackTrace(ex.getStackTrace());
            synchronized (exceptionMap) {
                exceptionMap.put(result, ex);
            }
        }
        return result;
    }

    private static PersistenceException adaptImpl(final net.sf.hibernate.HibernateException ex) {
        if (ex instanceof net.sf.hibernate.cache.CacheException)
            return adaptCacheException((net.sf.hibernate.cache.CacheException) ex);
        if (ex instanceof net.sf.hibernate.CallbackException)
            return new org.hibernate.CallbackException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof net.sf.hibernate.exception.ConstraintViolationException)
            return adaptConstraintException((net.sf.hibernate.exception.ConstraintViolationException) ex);
        if (ex instanceof net.sf.hibernate.exception.GenericJDBCException)
            return new org.hibernate.exception.GenericJDBCException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof net.sf.hibernate.id.IdentifierGenerationException)
            return new org.hibernate.id.IdentifierGenerationException(ex.getMessage(), ex.getCause());
        if (ex instanceof net.sf.hibernate.InstantiationException)
            return adaptInstantiationException((net.sf.hibernate.InstantiationException) ex);
        if (ex instanceof net.sf.hibernate.exception.JDBCConnectionException)
            return new org.hibernate.exception.JDBCConnectionException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof net.sf.hibernate.exception.LockAcquisitionException)
            return new org.hibernate.exception.LockAcquisitionException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof net.sf.hibernate.NonUniqueObjectException)
            return adaptNonUniqueObjectException((net.sf.hibernate.NonUniqueObjectException) ex);
        if (ex instanceof net.sf.hibernate.NonUniqueResultException)
            return adaptNonUniqueResultException((net.sf.hibernate.NonUniqueResultException) ex);
        if (ex instanceof net.sf.hibernate.ObjectDeletedException)
            return adaptObjectDeletedException((net.sf.hibernate.ObjectDeletedException) ex);
        if (ex instanceof net.sf.hibernate.ObjectNotFoundException)
            return adaptObjectNotFoundException((net.sf.hibernate.ObjectNotFoundException) ex);
        if (ex instanceof net.sf.hibernate.PersistentObjectException)
            return new org.hibernate.PersistentObjectException(ex.getMessage());
        if (ex instanceof net.sf.hibernate.PropertyAccessException)
            return adaptPropertyAccessException((net.sf.hibernate.PropertyAccessException) ex);
        if (ex instanceof net.sf.hibernate.PropertyNotFoundException)
            return new org.hibernate.PropertyNotFoundException(ex.getMessage());
        if (ex instanceof net.sf.hibernate.PropertyValueException)
            return adaptPropertyValueException((net.sf.hibernate.PropertyValueException) ex);
        if (ex instanceof net.sf.hibernate.QueryException)
            return adaptQueryException((net.sf.hibernate.QueryException) ex);
        if (ex instanceof net.sf.hibernate.exception.SQLGrammarException)
            return new org.hibernate.exception.SQLGrammarException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof net.sf.hibernate.type.SerializationException)
            return new org.hibernate.type.SerializationException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof net.sf.hibernate.StaleObjectStateException)
            return adaptStaleObjectStateException((net.sf.hibernate.StaleObjectStateException) ex);
        if (ex instanceof net.sf.hibernate.TransactionException)
            return new org.hibernate.TransactionException(ex.getMessage(), getCauseAsException(ex));
        if (ex instanceof net.sf.hibernate.TransientObjectException)
            return new org.hibernate.TransientObjectException(ex.getMessage());
        if (ex instanceof net.sf.hibernate.UnresolvableObjectException)
            return adaptUnresolvableObjectException((net.sf.hibernate.UnresolvableObjectException) ex);
        if (ex instanceof net.sf.hibernate.WrongClassException)
            return adaptWrongClassException((net.sf.hibernate.WrongClassException) ex);
        if (ex instanceof net.sf.hibernate.JDBCException)
            return new org.hibernate.JDBCException(ex.getMessage(), getSQLExceptionFromThrowable(ex.getCause()));
        if (ex instanceof net.sf.hibernate.MappingException)
            return new org.hibernate.MappingException(ex.getMessage(), ex.getCause());
        return new org.hibernate.HibernateException(ex.getMessage(), ex.getCause());
    }

    private static net.sf.hibernate.cache.CacheException adaptCacheException(
            final org.hibernate.cache.CacheException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.cache.CacheException(getCauseAsException(ex)) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.cache.CacheException.class);
            }
        };
    }

    private static org.hibernate.cache.CacheException adaptCacheException(
            final net.sf.hibernate.cache.CacheException ex) {
        return new org.hibernate.cache.CacheException(ex.getMessage(), ex.getCause());
    }

    private static net.sf.hibernate.exception.ConstraintViolationException adaptConstraintException(
            final org.hibernate.exception.ConstraintViolationException ex) {
        return new net.sf.hibernate.exception.ConstraintViolationException(
                ex.getMessage() + "; sql=" + ex.getSQL(),
                getSQLExceptionFromThrowable(ex.getCause()),
                ex.getConstraintName());
    }

    private static org.hibernate.exception.ConstraintViolationException adaptConstraintException(
            final net.sf.hibernate.exception.ConstraintViolationException ex) {
        return new org.hibernate.exception.ConstraintViolationException(
                ex.getMessage(),
                getSQLExceptionFromThrowable(ex.getCause()),
                ex.getConstraintName());
    }

    private static net.sf.hibernate.InstantiationException adaptInstantiationException(
            final org.hibernate.InstantiationException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.InstantiationException(ex.getMessage(), ex.getUninstantiatableClass(), ex.getCause()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.InstantiationException.class);
            }
        };
    }

    private static org.hibernate.InstantiationException adaptInstantiationException(
            final net.sf.hibernate.InstantiationException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.InstantiationException(ex.getMessage(), ex.getPersistentClass(), ex.getCause()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.InstantiationException.class);
            }
        };
    }

    private static net.sf.hibernate.NonUniqueObjectException adaptNonUniqueObjectException(
            final org.hibernate.NonUniqueObjectException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.NonUniqueObjectException(ex.getMessage(), ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.cache.CacheException.class);
            }
        };
    }

    private static org.hibernate.NonUniqueObjectException adaptNonUniqueObjectException(
            final net.sf.hibernate.NonUniqueObjectException ex) {

        final String message = ex.getMessage();
        final String entityName = ex.getPersistentClass() != null ? ex.getPersistentClass().getName() : null;
        return new org.hibernate.NonUniqueObjectException(ex.getMessage(), ex.getIdentifier(), entityName) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.NonUniqueObjectException.class);
            }
        };
    }

    private static net.sf.hibernate.NonUniqueObjectException adaptNonUniqueObjectException(
            final javax.persistence.EntityExistsException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.NonUniqueObjectException(ex.getMessage(), null, null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.cache.CacheException.class);
            }
        };
    }

    private static net.sf.hibernate.NonUniqueResultException adaptNonUniqueResultException(
            final PersistenceException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.NonUniqueResultException(-1) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.NonUniqueResultException.class);
            }
        };
    }

    private static org.hibernate.NonUniqueResultException adaptNonUniqueResultException(
            final net.sf.hibernate.NonUniqueResultException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.NonUniqueResultException(-1) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.NonUniqueResultException.class);
            }
        };
    }

    private static net.sf.hibernate.ObjectDeletedException adaptObjectDeletedException(
            final org.hibernate.ObjectDeletedException ex) {

        return adaptObjectDeletedException(ex.getMessage(), ex.getIdentifier());
    }

    private static net.sf.hibernate.ObjectDeletedException adaptObjectDeletedException(
            final String message, final Serializable identifier) {

        return new net.sf.hibernate.ObjectDeletedException(message, identifier, null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.ObjectDeletedException.class);
            }
        };
    }

    private static org.hibernate.ObjectDeletedException adaptObjectDeletedException(
            final net.sf.hibernate.ObjectDeletedException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.ObjectDeletedException(ex.getMessage(), ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.ObjectDeletedException.class);
            }
        };
    }

    private static net.sf.hibernate.ObjectNotFoundException adaptObjectNotFoundException(
            final org.hibernate.ObjectNotFoundException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.ObjectNotFoundException(ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.ObjectNotFoundException.class);
            }
        };
    }

    private static org.hibernate.ObjectNotFoundException adaptObjectNotFoundException(
            final net.sf.hibernate.ObjectNotFoundException ex) {

        final String message = ex.getMessage();
        final String entityName = ex.getPersistentClass() != null ? ex.getPersistentClass().getName() : null;
        return new org.hibernate.ObjectNotFoundException(ex.getIdentifier(), entityName) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.ObjectNotFoundException.class);
            }
        };
    }

    private static net.sf.hibernate.ObjectNotFoundException adaptObjectNotFoundException(
            final javax.persistence.EntityNotFoundException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.ObjectNotFoundException(null, null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.ObjectNotFoundException.class);
            }
        };
    }

    private static net.sf.hibernate.PropertyAccessException adaptPropertyAccessException(
            final org.hibernate.PropertyAccessException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.PropertyAccessException(
                ex.getCause(), ex.getMessage(), false, ex.getPersistentClass(), ex.getPropertyName()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.PropertyAccessException.class);
            }
        };
    }

    private static org.hibernate.PropertyAccessException adaptPropertyAccessException(
            final net.sf.hibernate.PropertyAccessException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.PropertyAccessException(
                ex.getCause(), ex.getMessage(), false, ex.getPersistentClass(), ex.getPropertyName()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.PropertyAccessException.class);
            }
        };
    }

    private static net.sf.hibernate.PropertyValueException adaptPropertyValueException(
            final org.hibernate.PropertyValueException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.PropertyValueException(ex.getMessage(), null, ex.getPropertyName()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.PropertyValueException.class);
            }
        };
    }

    private static org.hibernate.PropertyValueException adaptPropertyValueException(
            final net.sf.hibernate.PropertyValueException ex) {

        final String message = ex.getMessage();
        final String entityName = ex.getPersistentClass() != null ? ex.getPersistentClass().getName() : null;
        return new org.hibernate.PropertyValueException(ex.getMessage(), entityName, ex.getPropertyName()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.PropertyValueException.class);
            }
        };
    }

    private static net.sf.hibernate.QueryException adaptQueryException(
            final org.hibernate.QueryException ex) {

        final String message = ex.getMessage();
        final net.sf.hibernate.QueryException result = new net.sf.hibernate.QueryException(ex) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.QueryException.class);
            }
        };
        result.setQueryString(ex.getQueryString());
        return result;
    }

    private static org.hibernate.QueryException adaptQueryException(
            final net.sf.hibernate.QueryException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.QueryException(message, ex.getQueryString(), (Exception) ex.getCause()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.QueryException.class);
            }
        };
    }

    private static net.sf.hibernate.StaleObjectStateException adaptOptimisticLockException(
            final javax.persistence.OptimisticLockException ex) {

        if (ex.getCause() instanceof org.hibernate.StaleStateException) {
            return adaptStaleObjectStateException((org.hibernate.StaleStateException) ex.getCause());
        }

        final String message = ex.getMessage();
        final Class entityClass = ex.getEntity() != null ? ex.getEntity().getClass() : Object.class;
        return new net.sf.hibernate.StaleObjectStateException(entityClass, null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.StaleObjectStateException.class);
            }
        };
    }

    private static net.sf.hibernate.StaleObjectStateException adaptStaleObjectStateException(
            final org.hibernate.StaleStateException ex) {

        org.hibernate.StaleObjectStateException stex =
                ex instanceof org.hibernate.StaleObjectStateException
                ? (org.hibernate.StaleObjectStateException) ex
                : null;
        final String message = ex.getMessage();
        final String entityName = stex != null ? stex.getEntityName() : null;
        final Serializable identifier = stex != null ? stex.getIdentifier() : null;

        return new net.sf.hibernate.StaleObjectStateException(null, identifier) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public Class getPersistentClass() {
                try {
                    return entityName != null ? Class.forName(entityName) : Object.class;
                } catch (ClassNotFoundException ex) {
                    return Object.class;
                }
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.StaleObjectStateException.class);
            }
        };
    }

    private static org.hibernate.StaleObjectStateException adaptStaleObjectStateException(
            final net.sf.hibernate.StaleObjectStateException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.StaleObjectStateException(null, ex.getIdentifier()) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.StaleObjectStateException.class);
            }
        };
    }

    private static net.sf.hibernate.UnresolvableObjectException adaptUnresolvableObjectException(
            final org.hibernate.UnresolvableObjectException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.UnresolvableObjectException(ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.UnresolvableObjectException.class);
            }
        };
    }

    private static org.hibernate.UnresolvableObjectException adaptUnresolvableObjectException(
            final net.sf.hibernate.UnresolvableObjectException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.UnresolvableObjectException(ex.getMessage(), ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.UnresolvableObjectException.class);
            }
        };
    }

    private static net.sf.hibernate.WrongClassException adaptWrongClassException(
            final org.hibernate.WrongClassException ex) {

        final String message = ex.getMessage();
        return new net.sf.hibernate.WrongClassException(ex.getMessage(), ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, net.sf.hibernate.WrongClassException.class);
            }
        };
    }

    private static org.hibernate.WrongClassException adaptWrongClassException(
            final net.sf.hibernate.WrongClassException ex) {

        final String message = ex.getMessage();
        return new org.hibernate.WrongClassException(ex.getMessage(), ex.getIdentifier(), null) {
            @Override
            public String getMessage() {
                return message;
            }

            @Override
            public String toString() {
                return toStringWithClassName(this, org.hibernate.WrongClassException.class);
            }
        };
    }

    private static SQLException getSQLExceptionFromThrowable(final Throwable cause) {
        return
                cause instanceof SQLException
                ? (SQLException) cause
                : new SQLException(cause);
    }

    private static Exception getCauseAsException(final Exception ex) {
        return
                ex.getCause() == null || ex.getCause() instanceof Exception
                ? (Exception) ex.getCause()
                : ex;
    }

    /**
     * Copied from Throwable.toString().
     * Required to avoid using anonymous inner classes as the class name.
     */
    private static String toStringWithClassName(final Exception ex, final Class exceptionClass) {
        final String s = exceptionClass.getName();
        final String message = ex.getLocalizedMessage();
        return (message != null) ? (s + ": " + message) : s;
    }
}
