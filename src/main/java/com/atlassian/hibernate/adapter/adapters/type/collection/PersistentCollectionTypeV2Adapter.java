package com.atlassian.hibernate.adapter.adapters.type.collection;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.persister.CollectionPersisterV5Adapter;
import com.atlassian.hibernate.adapter.adapters.type.ForeignKeyDirectionV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.JoinableV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import com.atlassian.hibernate.adapter.type.V5TypeSupplier;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.Mapping;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.persister.Joinable;
import net.sf.hibernate.type.ForeignKeyDirection;
import net.sf.hibernate.type.PersistentCollectionType;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

/**
 * An adapter bridging the PersistentCollectionType class from hibernate v5 (onwards) to v2.
 */
public class PersistentCollectionTypeV2Adapter extends PersistentCollectionType {
    private final org.hibernate.type.CollectionType type;
    private final TypeV2Adapter typeAdapter;

    protected PersistentCollectionTypeV2Adapter(final org.hibernate.type.CollectionType type) {
        super(type.getRole());
        this.type = type;
        this.typeAdapter = new TypeV2Adapter(type);
    }

    public static PersistentCollectionType adapt(final org.hibernate.type.CollectionType type) {
        if (type == null)
            return null;
        if (type instanceof org.hibernate.type.ArrayType)
            return new ArrayTypeV2Adapter((org.hibernate.type.ArrayType) type);
        if (type instanceof org.hibernate.type.BagType)
            return new BagTypeV2Adapter((org.hibernate.type.BagType) type);
        if (type instanceof org.hibernate.type.IdentifierBagType)
            return new IdentifierBagTypeV2Adapter((org.hibernate.type.IdentifierBagType) type);
        if (type instanceof org.hibernate.type.ListType)
            return new ListTypeV2Adapter((org.hibernate.type.ListType) type);
        if (type instanceof org.hibernate.type.SortedMapType)
            return new SortedMapTypeV2Adapter((org.hibernate.type.SortedMapType) type);
        if (type instanceof org.hibernate.type.SortedSetType)
            return new SortedSetTypeV2Adapter((org.hibernate.type.SortedSetType) type);
        if (type instanceof org.hibernate.type.MapType)
            return new MapTypeV2Adapter((org.hibernate.type.MapType) type);
        if (type instanceof org.hibernate.type.SetType)
            return new SetTypeV2Adapter((org.hibernate.type.SetType) type);
        throw new NotImplementedException("Unsupported org.hibernate.type.CollectionType: " + type.getClass().getName());
    }

    public org.hibernate.type.Type getV5Type() {
        return type;
    }

    //---------- Object Overrides ----------//

    @Override
    public boolean equals(final Object object) {
        return
                object instanceof V5TypeSupplier
                && type.equals(((V5TypeSupplier) object).getV5Type());
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return type.toString();
    }

    //---------- Type Overrides ----------//

    @Override
    public boolean isAssociationType() {
        return typeAdapter.isAssociationType();
    }

    @Override
    public boolean isPersistentCollectionType() {
        return typeAdapter.isPersistentCollectionType();
    }

    @Override
    public boolean isComponentType() {
        return typeAdapter.isComponentType();
    }

    @Override
    public boolean isEntityType() {
        return typeAdapter.isEntityType();
    }

    @Override
    public boolean isObjectType() {
        return typeAdapter.isObjectType();
    }

    @Override
    public int[] sqlTypes(final Mapping session) throws MappingException {
        return typeAdapter.sqlTypes(session);
    }

    @Override
    public int getColumnSpan(final Mapping session) throws MappingException {
        return typeAdapter.getColumnSpan(session);
    }

    @Override
    public Class getReturnedClass() {
        return typeAdapter.getReturnedClass();
    }

    //public boolean equals(Object x, Object y);

    @Override
    public boolean isDirty(final Object old, final Object current, final SessionImplementor session)
            throws HibernateException {
        return typeAdapter.isDirty(old, current, session);
    }

    @Override
    public boolean isModified(
            final Object old,
            final Object current,
            final SessionImplementor session)
            throws HibernateException {
        return typeAdapter.isModified(old, current, session);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return typeAdapter.nullSafeGet(rs, name, session, owner);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return typeAdapter.nullSafeGet(rs, name, session, owner);
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session)
            throws HibernateException, SQLException {
        typeAdapter.nullSafeSet(st, value, index, session);
    }

    @Override
    public String toString(final Object value, final SessionFactoryImplementor factory) throws HibernateException {
        return typeAdapter.toString(value, factory);
    }

    @Override
    public Object fromString(final String xml) {
        return typeAdapter.fromString(xml);
    }

    @Override
    public String getName() {
        return typeAdapter.getName();
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        return typeAdapter.deepCopy(value);
    }

    @Override
    public boolean isMutable() {
        return typeAdapter.isMutable();
    }

    @Override
    public Serializable disassemble(final Object value, final SessionImplementor session)
            throws HibernateException {
        return typeAdapter.disassemble(value, session);
    }

    @Override
    public Object assemble(final Serializable cached, final SessionImplementor session, final Object owner)
            throws HibernateException {
        return typeAdapter.assemble(cached, session, owner);
    }

    @Override
    @Deprecated
    public boolean hasNiceEquals() {
        return typeAdapter.hasNiceEquals();
    }

    @Override
    public Object hydrate(final ResultSet rs, final String[] name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return typeAdapter.hydrate(rs, name, session, owner);
    }

    @Override
    public Object resolveIdentifier(final Object value, final SessionImplementor session, final Object owner)
            throws HibernateException {
        return typeAdapter.resolveIdentifier(value, session, owner);
    }

    @Override
    public Object copy(final Object original, final Object target, final SessionImplementor session, final Object owner, final Map copiedAlready)
            throws HibernateException {
        return typeAdapter.copy(original, target, session, owner, copiedAlready);
    }

    //---------- PersistentCollectionType Overrides ----------//

    @Override
    public String getRole() {
        return type.getRole();
    }

    @Override
    public PersistentCollection instantiate(final SessionImplementor session, final CollectionPersister persister) {
        final org.hibernate.engine.spi.SessionImplementor sessionV5 =
                (org.hibernate.engine.spi.SessionImplementor) HibernateBridge.get(session.getFactory()).getV5Session(session);
        return PersistentCollectionV2Adapter.adapt(
                type.instantiate(sessionV5, CollectionPersisterV5Adapter.adapt(persister, sessionV5.getFactory()), null));
    }

    @Override
    public Iterator getElementsIterator(final Object collection) {
        throw new NotImplementedException("getElementsIterator not implemented");
    }

    @Override
    public PersistentCollection wrap(final SessionImplementor session, final Object collection) {
        throw new NotImplementedException("wrap not implemented");
    }

    @Override
    public ForeignKeyDirection getForeignKeyDirection() {
        return ForeignKeyDirectionV2Adapter.adapt(type.getForeignKeyDirection());
    }

    @Override
    public boolean isArrayType() {
        return type.isArrayType();
    }

    @Override
    public boolean usePrimaryKeyAsForeignKey() {
        return true;
    }

    @Override
    public Joinable getJoinable(final SessionFactoryImplementor factory) throws MappingException {
        try {
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryV5 =
                    (org.hibernate.engine.spi.SessionFactoryImplementor) HibernateBridge.get(factory).getV5SessionFactory();
            return JoinableV2Adapter.adapt(type.getAssociatedJoinable(sessionFactoryV5), sessionFactoryV5);
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String[] getReferencedColumns(final SessionFactoryImplementor factory) throws MappingException {
        throw new NotImplementedException("getReferencedColumns not implemented");
    }

    @Override
    @SuppressWarnings("deprecation")
    public Class getAssociatedClass(final SessionFactoryImplementor factory) throws MappingException {
        try {
            final org.hibernate.SessionFactory sessionFactory = HibernateBridge.get(factory).getV5SessionFactory();
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryImpl =
                    (org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory;

            final String entityName = type.getAssociatedEntityName(sessionFactoryImpl);
            return sessionFactoryImpl.getEntityPersister(entityName).getMappedClass();
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }
}
