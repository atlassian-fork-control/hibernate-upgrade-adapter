package com.atlassian.hibernate.adapter.adapters.cache;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cache.spi.access.CollectionRegionAccessStrategy;

import java.util.function.Function;

/**
 * An adapter bridging the CollectionRegion interface for hibernate v2 to v5 (onwards).
 */
public class CollectionRegionV5Adapter extends TransactionDataRegionV5Adapter implements CollectionRegion {

    protected CollectionRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache,
            final Function<String, String> cacheStrategyClassSupplier,
            final CacheDataDescription metadata) {

        super(regionName, underlyingCache, metadata, cacheStrategyClassSupplier);
    }

    public static CollectionRegion adapt(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache,
            final Function<String, String> cacheStrategyClassSupplier,
            final CacheDataDescription metadata) {

        return new CollectionRegionV5Adapter(regionName, underlyingCache, cacheStrategyClassSupplier, metadata);
    }

    @Override
    public CollectionRegionAccessStrategy buildAccessStrategy(final AccessType accessType) throws CacheException {
        return CollectionRegionAccessStrategyV5Adapter.adapt(this, buildCacheConcurrencyStategy(accessType));
    }
}
