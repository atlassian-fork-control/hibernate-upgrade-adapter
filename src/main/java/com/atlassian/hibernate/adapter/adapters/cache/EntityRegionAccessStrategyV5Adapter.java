package com.atlassian.hibernate.adapter.adapters.cache;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.internal.DefaultCacheKeysFactory;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.access.EntityRegionAccessStrategy;
import org.hibernate.cache.spi.access.SoftLock;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.persister.entity.EntityPersister;

/**
 * An adapter bridging the EntityRegionAccessStrategy interface for hibernate v2 to v5 (onwards).
 */
public class EntityRegionAccessStrategyV5Adapter implements EntityRegionAccessStrategy {
    private final EntityRegion region;
    private final net.sf.hibernate.cache.CacheConcurrencyStrategy strategy;

    protected EntityRegionAccessStrategyV5Adapter(
            final EntityRegion region,
            final net.sf.hibernate.cache.CacheConcurrencyStrategy strategy) {

        this.region = region;
        this.strategy = strategy;
    }

    public static EntityRegionAccessStrategy adapt(
            final EntityRegion region,
            final net.sf.hibernate.cache.CacheConcurrencyStrategy strategy) {

        if (region == null) {
            return null;
        }
        return new EntityRegionAccessStrategyV5Adapter(region, strategy);
    }

    public net.sf.hibernate.cache.CacheConcurrencyStrategy getCacheConcurrencyStrategy() {
        return strategy;
    }

    @Override
    public EntityRegion getRegion() {
        return region;
    }

    @Override
    public Object get(final SharedSessionContractImplementor session, final Object key, final long txTimestamp) throws CacheException {
        try {
            return strategy.get(key, txTimestamp);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean putFromLoad(final SharedSessionContractImplementor session, final Object key, final Object value, final long txTimestamp, final Object version)
            throws CacheException {

        return putFromLoad(session, key, value, txTimestamp, version, false);
    }

    @Override
    public boolean putFromLoad(
            final SharedSessionContractImplementor session,
            final Object key,
            final Object value,
            final long txTimestamp,
            final Object version,
            final boolean minimalPutOverride) throws CacheException {

        if (minimalPutOverride) {
            throw new NotImplementedException("minimalPutOverride functionality not implemented");
        }

        try {
            return strategy.put(key, value, txTimestamp, version, region.getCacheDataDescription().getVersionComparator());
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public SoftLock lockItem(final SharedSessionContractImplementor session, final Object key, final Object version) throws CacheException {
        try {
            return SoftLockAdapter.adapt(strategy.lock(key, version));
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public SoftLock lockRegion() throws CacheException {
        return null;
    }

    @Override
    public void unlockItem(final SharedSessionContractImplementor session, final Object key, final SoftLock lock)
            throws CacheException {

        try {
            strategy.release(key, SoftLockAdapter.adapt(lock));
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void unlockRegion(final SoftLock lock) throws CacheException {
        if (region.isTransactionAware()) {
            return;
        }

        try {
            strategy.clear();
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean insert(final SharedSessionContractImplementor session, final Object key, final Object value, final Object version)
            throws CacheException {

        try {
            strategy.insert(key, value);
            return false; // this is not entirely accurate, but is only used for statistics
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean afterInsert(final SharedSessionContractImplementor session, final Object key, final Object value, final Object version)
            throws CacheException {

        try {
            strategy.afterInsert(key, value, version);
            return false; // this is not entirely accurate, but is only used for statistics
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean update(
            final SharedSessionContractImplementor session,
            final Object key,
            final Object value,
            final Object currentVersion,
            final Object previousVersion) throws CacheException {

        try {
            strategy.update(key, value);
            return false; // this is not entirely accurate, but is only used for statistics
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean afterUpdate(
            final SharedSessionContractImplementor session,
            final Object key,
            final Object value,
            final Object currentVersion,
            final Object previousVersion,
            final SoftLock lock) throws CacheException {

        try {
            strategy.afterUpdate(key, value, currentVersion, SoftLockAdapter.adapt(lock));
            return false; // this is not entirely accurate, but is only used for statistics
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void remove(final SharedSessionContractImplementor session, final Object key) throws CacheException {
        try {
            strategy.evict(key);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void removeAll() throws CacheException {
        try {
            strategy.clear();
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evict(final Object key) throws CacheException {
        try {
            strategy.remove(key);
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evictAll() throws CacheException {
        try {
            strategy.clear();
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object generateCacheKey(
            final Object id,
            final EntityPersister persister,
            final SessionFactoryImplementor factory,
            final String tenantIdentifier) {
        return id;
    }

    @Override
    public Object getCacheKeyId(final Object cacheKey) {
        return DefaultCacheKeysFactory.getEntityId(cacheKey);
    }
}
