package com.atlassian.hibernate.util;

public final class ThrowableUtil {
    private ThrowableUtil() { }

    /**
     * Allow any exception type to be thrown, by-passing checked exception verification.
     */
    public static RuntimeException propagateAll(final Throwable ex) {
        ThrowableUtil.<RuntimeException>propagateAllImpl(ex);
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> T propagateAllImpl(final Throwable ex) throws T {
        throw (T) ex;
    }
}
