package com.atlassian.hibernate.adapter.adapters.exception;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.google.common.base.Throwables;
import net.sf.hibernate.exception.ExceptionUtils;
import org.hibernate.internal.ExceptionConverterImpl;
import org.junit.Test;

import javax.persistence.PersistenceException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HibernateExceptionAdapterTest {

    private static void throwHibernateExceptionV2_AdaptedAndThrowNested() throws org.hibernate.HibernateException {
        try {
            throwHibernateExceptionV2_Adapted();
        } catch (final org.hibernate.HibernateException ex) {
            throw new org.hibernate.HibernateException(ex.getMessage(), ex);
        }
    }

    private static void throwHibernateExceptionV2_Adapted() throws org.hibernate.HibernateException {
        try {
            throwHibernateExceptionV2_Original();
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    private static void throwHibernateExceptionV2_Original() throws net.sf.hibernate.HibernateException {
        throw new net.sf.hibernate.HibernateException("badness");
    }

    private static void throwHibernateExceptionV5_AdaptedAndThrowNested() throws net.sf.hibernate.HibernateException {
        try {
            throwHibernateExceptionV5_Adapted();
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw new net.sf.hibernate.HibernateException(ex.getMessage(), ex);
        }
    }

    private static void throwHibernateExceptionV5_Adapted() throws net.sf.hibernate.HibernateException {
        try {
            throwHibernateExceptionV5_Original();
        } catch (final org.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    private static void throwHibernateExceptionV5_Original() throws org.hibernate.HibernateException {
        throw new org.hibernate.HibernateException("badness");
    }

    @Test
    public void testNonUniqueResultException_V5toV2() {
        final net.sf.hibernate.NonUniqueResultException ex = (net.sf.hibernate.NonUniqueResultException)
                HibernateExceptionAdapter.adapt(new org.hibernate.NonUniqueResultException(1234));
        assertEquals(true, ex.getMessage().endsWith(": 1234"));
    }

    @Test
    public void testUnresolvableObjectException_V5toV2() {
        final org.hibernate.UnresolvableObjectException v5exception =
                new org.hibernate.UnresolvableObjectException("message", "id", "entityName") { };
        final net.sf.hibernate.UnresolvableObjectException ex = (net.sf.hibernate.UnresolvableObjectException)
                HibernateExceptionAdapter.adapt(v5exception);

        assertEquals("message: [entityName#id]", ex.getMessage());
        assertEquals("id", ex.getIdentifier());
        assertEquals(null, ex.getPersistentClass());
    }

    @Test
    public void testStackTracePreservedV2() {
        try {
            throwHibernateExceptionV2_Adapted();
            fail("Expected an exception");
        } catch (final org.hibernate.HibernateException ex) {
            assertEquals(
                    "Original stack trace must be preserved for logging purposes",
                    true, ExceptionUtils.getFullStackTrace(ex).contains("throwHibernateExceptionV2_Original"));
        }
    }

    @Test
    public void testStackTracePreservedV2_RethrowNested() {
        try {
            throwHibernateExceptionV2_AdaptedAndThrowNested();
            fail("Expected an exception");
        } catch (final org.hibernate.HibernateException ex) {
            assertEquals(
                    "Original stack trace must be preserved for logging purposes",
                    true, ExceptionUtils.getFullStackTrace(ex).contains("throwHibernateExceptionV2_Original"));
        }
    }

    @Test
    public void testStackTracePreservedV5() {
        try {
            throwHibernateExceptionV5_Adapted();
            fail("Expected an exception");
        } catch (final net.sf.hibernate.HibernateException ex) {
            assertEquals(
                    "Original stack trace must be preserved for logging purposes",
                    true, ExceptionUtils.getFullStackTrace(ex).contains("throwHibernateExceptionV5_Original"));
        }
    }

    @Test
    public void testStackTracePreservedV5_RethrowNested() {
        try {
            throwHibernateExceptionV5_AdaptedAndThrowNested();
            fail("Expected an exception");
        } catch (final net.sf.hibernate.HibernateException ex) {
            assertEquals(
                    "Original stack trace must be preserved for logging purposes",
                    true, ExceptionUtils.getFullStackTrace(ex).contains("throwHibernateExceptionV5_Original"));
        }
    }

    @Test
    public void testAdapt_StaleStateException() {
        try {
            throw HibernateExceptionAdapter.adapt(new org.hibernate.StaleStateException("error"));
        } catch (final net.sf.hibernate.StaleObjectStateException ex) {
            assertEquals("error", ex.getMessage());
        } catch (net.sf.hibernate.HibernateException ex) {
            Throwables.propagate(ex);
        }
    }

    @Test
    public void testAdapt_StaleObjectStateException() {
        try {
            throw HibernateExceptionAdapter.adapt(new org.hibernate.StaleObjectStateException("error", "identifier"));
        } catch (final net.sf.hibernate.StaleObjectStateException ex) {
            assertEquals("Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect) : [error#identifier]", ex.getMessage());
        } catch (net.sf.hibernate.HibernateException ex) {
            Throwables.propagate(ex);
        }
    }

    @Test
    public void testAdapt_ConstraintViolationException() {
        net.sf.hibernate.HibernateException adapted;
        try {
            throw new ExceptionConverterImpl(null).convert(
                    new org.hibernate.exception.ConstraintViolationException("badness", new SQLException(""), null));
        } catch (PersistenceException ex) {
            adapted = HibernateExceptionAdapter.adapt(ex);
        }
        assertEquals(net.sf.hibernate.exception.ConstraintViolationException.class, adapted.getClass());
    }
}
