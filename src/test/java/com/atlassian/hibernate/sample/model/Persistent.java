package com.atlassian.hibernate.sample.model;

public class Persistent {
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(final Long value) {
		id = value;
	}

}
